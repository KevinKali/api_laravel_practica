<?php

namespace App\Http\Controllers\Country;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CountryModel;
use Validator;
use Illuminate\Support\Facades\Cache;

class Country extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(CountryModel::get(),200);
    }

    // con paginacion
    // public function index(){
    //     $countries  = Cache::remember('paisesCache', 15/60, function () {
    //         return CountryModel::simplePaginate(10);
    //     });

    //     return response()->json(['siguiente'=>$countries->nextPageUrl(),'anterior'=>$countries->previousPageUrl(),'data'=>$countries->items()],200);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:3',
            'iso' => 'required|min:2|max:2',
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        $country = CountryModel::create($request->all());
        return response()->json($country,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = CountryModel::find($id);

        if(is_null($country)){
            return response()->json(['message'=> "Record not found!"],404);
        }
        return response()->json($country,200);
    }

    // public function busca(Request $request, $name)
    // {
    //     // $country = CountryModel::where('name', 'LIKE', '%'.$name.'%')->get();
    //     // $country = CountryModel::where('name', 'LIKE', "%$name%")->get();
    //     $country = CountryModel::where('name', 'LIKE', "%$request->name%")->get();

    //     if($country->count() <= 0){
    //         return response()->json(['message'=> "archivo no encontrado"],404);
    //     }
    //     return response()->json($country,200);
    //     // return response()->json(CountryModel::where('name', 'LIKE', '%'.$name.'%')->get(),200);
    // }
    public function busca($name)
    {
        $country = CountryModel::where('name', 'LIKE', "%$name%")->get();

        if($country->count() <= 0){
            return response()->json(['message'=> "archivo no encontrado"],404);
        }
        return response()->json($country,200);
    }
    
    public function buscaTuani(Request $request){
        return response()->json(CountryModel::where('name', 'LIKE', "%{$request->name}%")->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $country = CountryModel::find($id);
        if(is_null($country)){
            return response()->json(['message'=> "Record, not found"],404);
        }
        $country->update($request->all());
        return response()->json($country,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = CountryModel::find($id);
        if(is_null($country)){
            return response()->json(['message'=> "Record, not found"],404);
        }
        $country->delete();
        return response()->json(null,204);
    }
}
