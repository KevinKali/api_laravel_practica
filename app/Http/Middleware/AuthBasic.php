<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthBasic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //acomode el handler del archivo exception para que no hubiera problemas en mostrar un blade
        if(Auth::onceBasic()){
            return response()->json(['message' => 'Auth Failed'], 401);
        }else{
            return $next($request);
        }
    }
}
