<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('country','Country\CountryController@country');
// Route::get('country/{id}','Country\CountryController@countryById');
// Route::post('country','Country\CountryController@countrySave');
// Route::put('country/{country}', 'Country\CountryController@countryUpdate');
// Route::delete('country/{country}', 'Country\CountryController@countryDelete');

Route::get('busca','Country\Country@buscaTuani');
Route::apiResource('country', 'Country\Country');
Route::get('country/busca/{name?}', 'Country\Country@busca')->name('country.busca');
// Route::group(['middleware'=>'auth:api'], function(){
//     Route::apiResource('country', 'Country\Country');
//     Route::get('country/busca/{name?}', 'Country\Country@busca')->name('country.busca');
// });

Route::fallback(function(){
    return response()->json([
        'message'=>'Page Not Found'
    ],404);
});